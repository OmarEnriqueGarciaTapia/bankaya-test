import React from 'react';

import {Router, Scene} from 'react-native-router-flux';
import {DefaultLayout, MainLayout} from '../components/layouts';
import {Home, E404, Pokemon} from '../components/pages';

const Component = () => (
  <Router>
    <MainLayout>
      <Scene key="home" title="Home" initial={true}>
        <DefaultLayout>
          <Home />
        </DefaultLayout>
      </Scene>
      <Scene key="pokemon" title="Pokemon">
        <DefaultLayout>
          <Pokemon />
        </DefaultLayout>
      </Scene>
      <Scene key="e404" title="E404">
        <DefaultLayout>
          <E404 />
        </DefaultLayout>
      </Scene>
    </MainLayout>
  </Router>
);

export default Component;
