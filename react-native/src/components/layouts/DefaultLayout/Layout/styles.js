export default {
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    // transition: 'all 0.25s ease-in-out',
    minHeight: '80vh',
    width: '100%',
    maxWidth: '100%',
  },
  main: {
    width: '100%',
    minHeight: '80vh',
    height: '100%',
    // backgroundPosition: 'center',
    // backgroundRepeat: 'no-repeat',
    // backgroundSize: 'cover',
  },
};
