import React from 'react';
import {StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import {Header, Footer} from '..';
import Styles from './styles.js';

const Component = props => {
  const {children} = props;
  return (
    <div className={styles.root}>
      <Header />
      <div className={styles.main}>{children}</div>
      <Footer />
    </div>
  );
};

const styles = StyleSheet.create(Styles);

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {
  children: null,
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

export default Component;
