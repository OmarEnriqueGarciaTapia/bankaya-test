export default {
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '10vh',
    backgroundColor: '#F6CB09',
    borderBottomWidth: 1,
    borderBottomColor: '#3C5BA6',
  },
  logo: {
    position: 'absolute',
  },
  'logo > img ': {
    height: '9vh',
  },
  menu: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    width: '100%',
    height: '10vh',
  },
  left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '50%',
    height: '10vh',
  },
  right: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width: '50%',
    height: '10vh',
  },
  'right > div ': {
    margin: '0 10px',
  },
};
