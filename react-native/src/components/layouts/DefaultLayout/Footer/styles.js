export default {
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    margin: 'auto',
    height: '10vh',
    backgroundColor: '#F6CB09',
    borderTopWidth: 1,
    borderTopColor: '#3C5BA6',
  },
};
