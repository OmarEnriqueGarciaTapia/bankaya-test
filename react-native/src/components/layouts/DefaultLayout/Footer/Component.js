import React from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {Text} from '../../../ui';
import Styles from './styles.js';

const Component = () => {
  return (
    <div className={styles.root}>
      <Text text="Developed by Omar Garcia ´https://www.linkedin.com/in/ogarciatapia´." />
    </div>
  );
};

const styles = StyleSheet.create(Styles);

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
};

const defaultProps = {
  classes: {},
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

const mapStateToProps = () => ({});

const ComponentConnected = connect(mapStateToProps)(Component);

export default ComponentConnected;
