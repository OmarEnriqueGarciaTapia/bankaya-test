import {bindActionCreators} from 'redux';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {useLocation} from 'react-router-dom';
import PropTypes from 'prop-types';
import React, {useEffect, useState} from 'react';

import {scrollTop} from '../../../../utils/helpers';
// eslint-disable-next-line no-unused-vars
import {Loading, Messages} from '../../../ui';
import {
  deleteMessage as deleteMessageAction,
  deleteErrorMessage as deleteErrorMessageAction,
} from '../../../../store/actions';
import Styles from './styles.js';

const Component = props => {
  const {
    children,
    // eslint-disable-next-line no-unused-vars
    errorMessages,
    // eslint-disable-next-line no-unused-vars
    messages,
    fetchings,
    // eslint-disable-next-line no-unused-vars
    deleteErrorMessage,
    // eslint-disable-next-line no-unused-vars
    deleteMessage,
  } = props;

  const [loading, setLoading] = useState(false);
  const [rendering, setRendering] = useState(true);
  const location = useLocation();

  useEffect(() => {
    scrollTop();
  }, [location]);

  useEffect(() => {
    setTimeout(() => {
      setRendering(false);
    }, 1000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    let isLoading = false;
    fetchings.forEach(data => {
      if (data.fetching) {
        isLoading = true;
      }
    });
    setLoading(isLoading);
  }, [fetchings]);

  // const handleOnClick = (id, type) => {
  //   switch (type) {
  //     case 'success':
  //       deleteMessage(id);
  //       break;
  //     case 'error':
  //       deleteErrorMessage(id);
  //       break;
  //     default:
  //       break;
  //   }
  // };

  return (
    <div className={styles.root}>
      <div className={styles.background} />
      {/* {Object.keys(errorMessages).length > 0 && (
        <Messages
          messages={errorMessages}
          type="error"
          onClick={id => handleOnClick(id, 'error')}
        />
      )} */}
      {children}
      {/* {Object.keys(messages).length > 0 && (
        <Messages
          messages={messages}
          type="success"
          onClick={id => handleOnClick(id, 'success')}
        />
      )} */}
      {loading && <Loading />}
      {rendering && <Loading variant="simple" />}
    </div>
  );
};

const styles = StyleSheet.create(Styles);

const propTypes = {
  children: PropTypes.node,
  // messages: PropTypes.objectOf(PropTypes.string),
  // errorMessages: PropTypes.objectOf(PropTypes.object),
  // deleteMessage: PropTypes.func,
  // deleteErrorMessage: PropTypes.func,
  fetchings: PropTypes.arrayOf(PropTypes.object),
};

const defaultProps = {
  children: null,
  // messages: {},
  // errorMessages: {},
  // profile: null,
  // deleteMessage: null,
  // deleteErrorMessage: null,
  fetchings: [],
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

const mapStateToProps = state => ({
  errorMessages: state.errorMessages,
  messages: state.messages,
  fetchings: [
    {
      fetching: state.pokemons.fetching,
    },
    {
      fetching: state.pokemon.fetching,
    },
    {
      fetching: state.evolutions.fetching,
    },
  ],
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteMessage: deleteMessageAction,
      deleteErrorMessage: deleteErrorMessageAction,
    },
    dispatch,
  );

const ComponentConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default ComponentConnected;
