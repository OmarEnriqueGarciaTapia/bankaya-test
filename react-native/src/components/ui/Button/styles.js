export default {
  root: {
    borderRadius: 6,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // transition: 'all 025s ease-in-out',
    cursor: 'pointer',
    maxWidth: '100%',
  },
  text: {
    fontWeight: 600,
    fontSize: 15,
    lineHeight: 18,
    textAlign: 'center',
    color: '#ffffff',
  },
  'variant-normal': {
    width: 400,
    height: 54,
    backgroundColor: '#3C5BA6',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: 'transparent',
  },
  'variant-normal:hover': {
    backgroundColor: '#333333',
  },
  'variant-small': {
    width: 310,
    height: 54,
    backgroundColor: '#ffffff',
    // boxSizing: 'border-box',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: 'transparent',
  },
  'variant-small:hover': {
    backgroundColor: '#3C5BA6',
  },
  'state-disabled': {
    backgroundColor: '#fff',
    borderWidth: 0,
    cursor: 'default',
  },
  'state-disabled:hover': {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#cccccc',
    cursor: 'default',
  },
  'variant-small-state-active': {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#3C5BA6',
    // boxSizing: 'border-box',
    borderRadius: 6,
  },
  'variant-small-state-active:hover': {
    backgroundColor: '#3C5BA6',
    borderWidth: 1,
    borderColor: '#3C5BA6',
  },
  'variant-normal-state-selected': {
    borderWidth: 1,
    borderColor: '#cccccc',
    backgroundColor: '#888888',
  },
  'variant-small-state-selected': {
    backgroundColor: '#cccccc',
  },
  /* Text classes */
  'variant-small-state-active text-color-white': {
    color: '#3C5BA6',
  },
  'variant-small-state-active:hover text-color-white': {
    color: '#ffffff',
  },
  'variant-small-state-selected text-color-white': {
    color: '#3C5BA6',
  },
  'variant-small-state-selected:hover text-color-white': {
    color: '#ffffff',
  },
  'variant-normal-state-disabled text-color-white': {
    color: '#cccccc',
  },
  'variant-small-state-disabled text-color-white': {
    color: '#cccccc',
  },
};
