export default {
  root: {
    // transition: 'all 0.25s ease-in-out',
    fontFamily: 'Inter',
    WebkitFontSmoothing: 'antialiased',
    MozOsxFontSmoothing: 'grayscale',
    display: 'flex',
    width: 'fit-content',
    fontDisplay: 'block',
  },
  'children-position-right': {
    flexDirection: 'row',
    alignItems: 'center',
  },
  'children-position-left': {
    flexDirection: 'row-reverse',
    alignItems: 'center',
  },
  'children-position-top': {
    flexDirection: 'column-reverse',
    justifyContent: 'center',
    alignItems: 'center',
  },
  'children-position-bottom': {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  'font-Inter': {
    fontFamily: 'Inter',
    fontDisplay: 'block',
  },
  'variant-h1': {
    fontFamily: 'Inter-Bold',
    fontSize: '3em',
  },
  'variant-h2': {
    fontFamily: 'Inter-Bold',
    fontSize: '4em',
  },
  'variant-h3': {
    fontFamily: 'Inter-Bold',
    fontSize: '5em',
  },
  'variant-body': {
    fontFamily: 'Inter',
    fontSize: '2em',
  },
  'variant-bold': {
    fontFamily: 'Inter-SemiBold',
    fontSize: '2.1em',
  },
  'variant-a': {
    composes: 'variant-bold',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  html: {
    fontFamily: 'Inter',
    color: '#222222',
  },
  'html > p': {
    fontFamily: 'Inter',
    fontSize: 15,
    lineHeight: 22,
  },
  'color-white': {
    color: '#ffffff',
  },
  'color-black': {
    color: '#000000',
  },
  'color-dark': {
    color: '#222222',
  },
  'color-gray': {
    color: '#888888',
  },
  'color-light': {
    color: '#cccccc',
  },
  'color-blue': {
    color: '#3C5BA6',
  },

  'color-yellow': {
    color: '#F6CB09',
  },
  'color-error': {
    color: '#FF8787',
  },
};
