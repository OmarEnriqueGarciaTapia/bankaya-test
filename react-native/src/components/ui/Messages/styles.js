export default {
  root: {
    pointerEvents: 'painted',
    position: 'relative',
    backgroundColor: 'rgba(0, 0, 0, 08)',
    boxShadow: '0px 10px 20px rgba(0, 0, 0, 015)',
    borderRadius: 6,
    zIndex: 1,
  },
  'root-type-error': {
    top: 10,
    color: '#FF8787',
  },
  'root-type-success': {
    top: 10,
  },
  message: {
    width: 380,
    height: 'auto',
    padding: '0 30px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    cursor: 'pointer',
  },
  messageColumn: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    height: 82,
  },
  title: {
    marginBottom: 4,
    textTransform: 'capitalize',
  },
};
