export default {
  /*@keyframes Rotation: {
    0%: {
      transform: 'rotate(0deg)',
      opacity: 1,
    },
    50%: {
      transform: 'rotate(180deg)',
      opacity: 0.5,
    },
    100%: {
      transform: 'rotate(360deg)',
      opacity: 1,
    },
  },
  @keyframes Ease: {
    0%: {
      opacity: 1,
    },
    100%: {
      opacity: 0,
    },
  },*/
  root: {
    // pointerEvents: 'bounding-box',
    top: 0,
    margin: 0,
    padding: 0,
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'wait',
  },
  'root-simple': {
    backgroundColor: 'rgb(255, 255, 255)',
    animationName: 'Ease',
    animationTimingFunction: 'ease-in-out',
    animationIterationCount: 'infinite',
    animationDuration: '1s',
  },
  'loading-simple': {
    opacity: 1,
  },
  'root-circle': {
    backgroundColor: 'rgba(0, 0, 0, 09)',
  },
  'loading-circle': {
    borderWidth: 100,
    borderColor: '#888',
    borderRadius: '50%',
    borderTop: '25em solid rgba(0, 0, 0, 01)',
    width: '10em',
    height: '10em',
    animationName: 'Rotation',
    animationTimingFunction: 'linear',
    animationIterationCount: 'infinite',
    animationDuration: '075s',
  },
  'root-hidden': {
    opacity: 1,
  },
  'loading-hidden': {
    opacity: 1,
  },
};
