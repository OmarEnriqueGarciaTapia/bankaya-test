import React, {memo} from 'react';
import {StyleSheet} from 'react-native';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {Text, Button} from '..';
import Styles from './styles.js';
import PokemonCard from '../PokemonCard';

const Component = props => {
  const {classes, onClickClose, onClickCard, chain} = props;

  const classNameRoot = classNames(styles.root, classes.root);

  const handleOnClickClose = () => {
    if (onClickClose) {
      onClickClose();
    }
  };

  const handleOnClickCard = id => {
    if (onClickCard && id < 152) {
      onClickCard(id);
    }
  };

  const getId = url => {
    return url
      .replace('https://pokeapi.co/api/v2/pokemon-species/', '')
      .replace('/', '');
  };

  const getEvolves = () => {
    if (chain !== undefined) {
      const {evolves_to} = chain;
      if (evolves_to !== undefined) {
        return evolves_to.map(({species}) => {
          return {
            id: getId(species.url),
            name: species.name,
          };
        });
      }
    }
    return [];
  };

  return (
    <div className={classNameRoot}>
      {
        <div className={styles.content}>
          {chain && (
            <div className={styles.evolves}>
              {getEvolves().length > 0 ? (
                getEvolves().map(({id, name}) => {
                  return (
                    <PokemonCard
                      key={name}
                      name={name}
                      id={id}
                      onClick={() => handleOnClickCard(id)}
                    />
                  );
                })
              ) : (
                <Text text="It has no evolutions" variant="h3" />
              )}
            </div>
          )}
          <Button text="Close" onClick={() => handleOnClickClose()} />
        </div>
      }
    </div>
  );
};

const styles = StyleSheet.create(Styles);

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  chain: PropTypes.objectOf(PropTypes.object),
  onClickClose: PropTypes.func,
  onClickCard: null,
};

const defaultProps = {
  classes: {},
  chain: {},
  onClickClose: PropTypes.func,
  onClickCard: null,
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

export default memo(Component);
