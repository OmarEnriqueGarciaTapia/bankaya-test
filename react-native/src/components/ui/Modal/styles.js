export default {
  root: {
    // pointerEvents: 'bounding-box',
    top: 0,
    left: 0,
    margin: 0,
    padding: 0,
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100vh',
    backgroundColor: 'rgba(0,0,0,0.70)',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'default',
    zIndex: 1,
  },
  content: {
    width: '75%',
    height: '75vh',
    padding: 20,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderWidth: 1,
    borderColor: '#3C5BA6',
    borderRadius: 10,
    backgroundColor: '#F6CB09',
  },
  evolves: {
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
};
