export default {
  root: {
    width: '100%',
    height: 1,
  },
  normal: {
    margin: '20px 0',
    backgroundColor: '#EEEEEE',
  },
  withoutMargins: {
    margin: 0,
    backgroundColor: '#EEEEEE',
  },
  withoutTopMargin: {
    marginBottom: 20,
    backgroundColor: '#EEEEEE',
  },
  withoutBottomMargin: {
    marginTop: 20,
    backgroundColor: '#EEEEEE',
  },
  hidden: {
    margin: 0,
    backgroundColor: 'rgba(255, 255, 255, 0)',
  },
};
