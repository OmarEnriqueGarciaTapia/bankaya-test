export default {
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100vh',
    backgroundColor: 'gray',
  },
  catalog: {
    width: 'calc(100% - 40px)',
    height: 'auto',
    // overflowX: 'auto',
    overflow: 'scroll',
    // whiteSpace: 'nowrap',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
};
