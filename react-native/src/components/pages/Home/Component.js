import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {useHistory} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {getPokemons as getPokemonsAction} from '../../../store/actions';
import {PokemonCard} from '../../ui';
import Styles from './styles.js';

const Component = props => {
  const {getPokemons, pokemons} = props;

  const history = useHistory();

  useEffect(() => {
    getPokemons(151);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleOnClickCard = id => {
    history.push(`/pokemon/${id}`);
  };

  return (
    <div className={styles.root}>
      {pokemons.results && pokemons.results.length > 0 && (
        <div className={styles.catalog}>
          {pokemons.results.map((data, index) => {
            const id = index + 1;
            return (
              <PokemonCard
                key={data.name}
                name={data.name}
                id={id}
                onClick={() => handleOnClickCard(id)}
              />
            );
          })}
        </div>
      )}
    </div>
  );
};

const styles = StyleSheet.create(Styles);

const propTypes = {
  getPokemons: PropTypes.func,
  pokemons: PropTypes.arrayOf(PropTypes.object),
};

const defaultProps = {
  getPokemons: null,
  pokemons: [],
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

const mapStateToProps = state => ({
  pokemons: state.pokemons,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPokemons: getPokemonsAction,
    },
    dispatch,
  );

const ComponentConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default ComponentConnected;
