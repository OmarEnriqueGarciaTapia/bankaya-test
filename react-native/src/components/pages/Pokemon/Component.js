import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {useHistory, useLocation} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  getPokemon as getPokemonAction,
  getEvolutions as getEvolutionsAction,
} from '../../../store/actions';
import {getPathId} from '../../../utils/helpers';
import {Text, Button, Modal} from '../../ui';
import {capitalize} from '../../../utils/helpers';
import {endpoints, evolutionChain} from '../../../utils/constants';
import Styles from './styles.js';

const Component = props => {
  const {getPokemon, getEvolutions, pokemon, evolutions} = props;

  const history = useHistory();
  const location = useLocation();
  const id = getPathId(history);

  const [modal, setModal] = useState(false);

  useEffect(() => {
    getPokemon(id);
    getEvolutions(evolutionChain[id]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getPokemon(id);
    getEvolutions(evolutionChain[id]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  const getImage = () => {
    return endpoints.imagesApi.replace('{pokemon}', id);
  };

  const getImages = sprites => {
    let urls = [];
    const getUrls = data => {
      if (data !== null) {
        Object.keys(data).forEach(sprite => {
          const spriteData = data[sprite];
          if (spriteData !== null && typeof spriteData !== 'object') {
            urls = [...urls, spriteData];
          } else {
            getUrls(spriteData);
          }
        });
      }
    };
    getUrls(sprites);
    return urls;
  };

  const handleOnClickSeeEvolutions = () => {
    setModal(true);
  };

  const handleOnClickCard = id => {
    setModal(false);
    history.push(`/pokemon/${id}`);
  };

  return (
    <div className={styles.root}>
      {modal && evolutions.chain && (
        <Modal
          chain={evolutions.chain}
          onClickClose={() => setModal(false)}
          onClickCard={id => handleOnClickCard(id)}
        />
      )}
      {pokemon !== undefined && pokemon.name !== undefined && (
        <>
          <div className={styles.left}>
            <div className={styles.image}>
              <img src={getImage()} alt="Pokemon" />
            </div>
            <Text text="Sprites:" variant="h1" />
            <div className={styles.sprites}>
              {pokemon.sprites &&
                getImages(pokemon.sprites).map(sprite => {
                  return <img src={sprite} alt="sprite" />;
                })}
            </div>
          </div>
          <div className={styles.right}>
            <div className={styles.row}>
              <Text text={`Name: ${capitalize(pokemon.name)}`} variant="h3" />
            </div>
            <div className={styles.row}>
              <Button
                text="See evolutions"
                variant="normal"
                onClick={() => handleOnClickSeeEvolutions()}
              />
            </div>
            <div className={styles.block}>
              <Text text="General Info:" variant="h1" />
              <div className={styles.blockList}>
                <Text text={`height: ${pokemon.height}`} />
                <Text text={`weight: ${pokemon.weight}`} />
              </div>
            </div>
            {pokemon.abilities && (
              <div className={styles.block}>
                <Text text="Habilities:" variant="h1" />
                <div className={styles.blockList}>
                  {pokemon.abilities.map(({ability}) => {
                    return <Text text={ability.name} />;
                  })}
                </div>
              </div>
            )}
            {pokemon.moves && (
              <div className={styles.block}>
                <Text text="Moves:" variant="h1" />
                <div className={styles.blockList}>
                  {pokemon.moves.map(({move}) => {
                    return <Text text={move.name} />;
                  })}
                </div>
              </div>
            )}
            {pokemon.held_items && pokemon.held_items.length > 0 && (
              <div className={styles.block}>
                <Text text="Held Items:" variant="h1" />
                <div className={styles.blockList}>
                  {pokemon.held_items.map(({item}) => {
                    return <Text text={item.name} />;
                  })}
                </div>
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
};

const styles = StyleSheet.create(Styles);

const propTypes = {
  getPokemon: PropTypes.func,
  getEvolutions: PropTypes.func,
  pokemon: PropTypes.objectOf(PropTypes.object),
  evolutions: PropTypes.objectOf(PropTypes.object),
};

const defaultProps = {
  getPokemon: null,
  getEvolutions: null,
  pokemon: {},
  evolutions: {},
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

const mapStateToProps = state => ({
  pokemon: state.pokemon,
  evolutions: state.evolutions,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPokemon: getPokemonAction,
      getEvolutions: getEvolutionsAction,
    },
    dispatch,
  );

const ComponentConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);

export default ComponentConnected;
