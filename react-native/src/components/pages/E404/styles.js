export default {
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100vh',
    backgroundColor: '#FFF',
  },
  e404Box: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 'auto',
    height: 'auto',
    // boxSizing: 'border-box',
  },
  'e404Box > div': {
    marginRight: 20,
  },
};
