const endpoints = {
  api: 'https://pokeapi.co/api/v2',
  imagesApi: 'https://pokeres.bastionbot.org/images/pokemon/{pokemon}.png'
};

export default endpoints;