import {combineReducers} from 'redux';
import {
  ADD_ERROR_MESSAGE,
  ADD_MESSAGE,
  DELETE_ERROR_MESSAGE,
  DELETE_MESSAGE,
  FETCH_GET_POKEMONS,
  GET_POKEMONS,
  FETCH_GET_POKEMON,
  GET_POKEMON,
  FETCH_GET_EVOLUTIONS,
  GET_EVOLUTIONS,
} from '../actions';

const initialState = {};

function messages(state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case ADD_MESSAGE:
      return {
        ...state,
        ...payload,
      };
    case DELETE_MESSAGE:
      // eslint-disable-next-line no-case-declarations
      const {[payload]: deleted, ...others} = state;
      return {
        ...others,
      };
    default:
      return state;
  }
}

function errorMessages(state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case ADD_ERROR_MESSAGE:
      return {
        ...state,
        ...payload,
      };
    case DELETE_ERROR_MESSAGE:
      // eslint-disable-next-line no-case-declarations
      const {[payload]: deleted, ...others} = state;
      return {
        ...others,
      };
    default:
      return state;
  }
}

function pokemons(state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case FETCH_GET_POKEMONS:
      return {
        ...state,
        fetching: true,
      };
    case GET_POKEMONS:
      return {
        ...state,
        ...payload,
        fetching: false,
      };
    default:
      return state;
  }
}

function pokemon(state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case FETCH_GET_POKEMON:
      return {
        ...state,
        fetching: true,
      };
    case GET_POKEMON:
      return {
        ...state,
        ...payload,
        fetching: false,
      };
    default:
      return state;
  }
}

function evolutions(state = initialState, action) {
  const {payload} = action;
  switch (action.type) {
    case FETCH_GET_EVOLUTIONS:
      return {
        ...state,
        fetching: true,
      };
    case GET_EVOLUTIONS:
      return {
        ...state,
        ...payload,
        fetching: false,
      };
    default:
      return state;
  }
}

const reducers = combineReducers({
  messages,
  errorMessages,
  pokemons,
  pokemon,
  evolutions,
});

export default reducers;
