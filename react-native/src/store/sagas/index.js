import {call, put, takeEvery, delay} from 'redux-saga/effects';
import axios from 'axios';
import {
  FETCH_MESSAGE,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  ADD_ERROR_MESSAGE,
  DELETE_ERROR_MESSAGE,
  FETCH_GET_POKEMONS,
  GET_POKEMONS,
  GET_POKEMON,
  FETCH_GET_POKEMON,
  GET_EVOLUTIONS,
  FETCH_GET_EVOLUTIONS,
} from '../actions';

import constants from '../../utils/constants/endpoints';

const httpClient = axios.create();
httpClient.defaults.timeout = 6000;

const getHeaders = (newHeaders = {}) => {
  const headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Access-Control-Allow-Origin': '*',
    ...newHeaders,
  };
  return headers;
};

const fetch = config => {
  const {method, url, data, headers} = config;
  const path = `${constants.api}/${url}`;
  const newConfig = {
    method,
    url: path,
    data,
    headers: getHeaders(headers),
  };
  return httpClient(newConfig)
    .then(res => res.data)
    .catch(error => {
      let newData = {success: false};
      if (error.response && error.response.data) {
        newData = {
          ...newData,
          ...error.response.data,
        };
      } else if (error.request) {
        newData = {
          ...newData,
          ...error.request,
        };
      } else {
        newData = {...newData, message: 'Sin respuesta.'};
      }
      return newData;
    });
};

export function* fetchMessage({payload}) {
  const {id, message} = payload;
  yield put({type: DELETE_MESSAGE, payload: [id]});
  yield put({type: DELETE_ERROR_MESSAGE, payload: [id]});
  const success = payload.success ?? payload.success === true;
  const typeNewMessage = success ? ADD_MESSAGE : ADD_ERROR_MESSAGE;
  const typeDeleteMessage = success ? DELETE_MESSAGE : DELETE_ERROR_MESSAGE;
  yield put({
    type: typeNewMessage,
    payload: {
      [id]: message,
    },
  });
  yield delay(constants.deleteMessageDelay);
  yield put({type: typeDeleteMessage, payload: [id]});
}

export function* fetchGetPokemons({payload}) {
  const url = `pokemon?limit=${payload}`;
  const config = {
    method: 'get',
    url,
  };
  const response = yield call(() => fetch(config));
  yield put({type: GET_POKEMONS, payload: response});
  yield put({
    type: FETCH_MESSAGE,
    payload: {
      ...response,
      id: 'pokemons',
    },
  });
}

export function* fetchGetPokemon({payload}) {
  const url = `pokemon/${payload}`;
  const config = {
    method: 'get',
    url,
  };
  const response = yield call(() => fetch(config));
  yield put({type: GET_POKEMON, payload: response});
  yield put({
    type: FETCH_MESSAGE,
    payload: {
      ...response,
      id: 'pokemon',
    },
  });
}

export function* fetchGetEvolutions({payload}) {
  const url = `evolution-chain/${payload}`;
  const config = {
    method: 'get',
    url,
  };
  const response = yield call(() => fetch(config));
  yield put({type: GET_EVOLUTIONS, payload: response});
  yield put({
    type: FETCH_MESSAGE,
    payload: {
      ...response,
      id: 'evolutions',
    },
  });
}

function* watchActions() {
  yield takeEvery(FETCH_MESSAGE, fetchMessage);
  yield takeEvery(FETCH_GET_POKEMONS, fetchGetPokemons);
  yield takeEvery(FETCH_GET_POKEMON, fetchGetPokemon);
  yield takeEvery(FETCH_GET_EVOLUTIONS, fetchGetEvolutions);
}

export default watchActions;
