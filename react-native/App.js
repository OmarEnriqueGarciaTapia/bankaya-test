/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Provider} from 'react-redux';
import store from './src/store';
import Router from './src/router';
import type {Node} from 'react';

const App: () => Node = () => {
  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
};

export default App;
