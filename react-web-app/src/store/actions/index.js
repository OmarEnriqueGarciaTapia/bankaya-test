export const FETCH_MESSAGE = 'FETCH_MESSAGE';
export const ADD_MESSAGE = 'ADD_MESSAGE';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
export const ADD_ERROR_MESSAGE = 'ADD_ERROR_MESSAGE';
export const DELETE_ERROR_MESSAGE = 'DELETE_ERROR_MESSAGE';

export const FETCH_GET_POKEMONS = 'FETCH_GET_POKEMONS';
export const GET_POKEMONS = 'GET_POKEMONS';

export const FETCH_GET_POKEMON = 'FETCH_GET_POKEMON';
export const GET_POKEMON = 'GET_POKEMON';

export const FETCH_GET_EVOLUTIONS = 'FETCH_GET_EVOLUTIONS';
export const GET_EVOLUTIONS = 'GET_EVOLUTIONS';

export function addMessage(payload) {
  return {
    type: ADD_MESSAGE,
    payload
  };
}

export function addErrorMessage(payload) {
  return {
    type: ADD_ERROR_MESSAGE,
    payload
  };
}

export function deleteMessage(payload) {
  return {
    type: DELETE_MESSAGE,
    payload
  };
}

export function deleteErrorMessage(payload) {
  return {
    type: DELETE_ERROR_MESSAGE,
    payload
  };
}

export function getPokemons(payload) {
  return {
    type: FETCH_GET_POKEMONS,
    payload
  };
}

export function getPokemon(payload) {
  return {
    type: FETCH_GET_POKEMON,
    payload
  };
}

export function getEvolutions(payload) {
  return {
    type: FETCH_GET_EVOLUTIONS,
    payload
  };
}