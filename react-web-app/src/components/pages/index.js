export { default as E404 } from './E404';
export { default as Home } from './Home';
export { default as Pokemon } from './Pokemon';
