import React from 'react';
import renderer from 'react-test-renderer';
import { findByTestAtrr } from './../../../utils/helpers';
import ButtonWrapper, { Component as Button, defaultProps } from './Button';

const setup = (props = {}) => {
  const component =  renderer.create(
    <Button {...props} />
  );
  return component;
};

describe('Button', () => {
  const wrapper = renderer.create(
    <ButtonWrapper {...defaultProps} />
  );
  let tree = wrapper.toJSON();
  it('Button wrapped', () => {
    expect(tree).toMatchSnapshot();
  });
  // describe('Button test', () => {
  //   let wrapper;
  //   beforeEach(() => {
  //     const props = {
  //       ...defaultProps,
  //       classes: { default_selected: 'default_selected' }
  //     };
  //     wrapper = setup(props);
  //   });
  //   it('Should render without errors', () => {
  //     const component = findByTestAtrr(tree, 'buttonComponent');
  //     expect(component.length).toBe(1);
  //     // TODO: create tests once ezyme supports hooks.
  //     component.simulate('mouseDown');
  //     component.simulate('mouseUp');
  //     component.simulate('touchStart');
  //     component.simulate('touchEnd');
  //   });
  //   it(`Should render with this props: ${
  //     '(context, color)'} ${
  //     'without errors'}`, () => {
  //     wrapper.setProps({
  //       ...defaultProps,
  //       classes: {},
  //       variant: 'small',
  //       color: 'default'
  //     });
  //   });
  // });
});
