import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Styles.module.css';
import Text from '../Text';
import { endpoints } from '../../../utils/constants';

const Component = (props) => {
  const {
    classes,
    onClick,
    id,
    name
  } = props;

  const classNameRoot = classNames(
    styles.root,
    classes.root
  );

  const handlerOnClick = () => {
    if (onClick) onClick();
  };

  const getImage = () => {
    return endpoints.imagesApi.replace('{pokemon}', id);
  };

  return (
    <div
      className={classNameRoot}
      onClick={() => handlerOnClick()}
    >
      <div className={styles.image} style={{ backgroundImage: `url(${getImage()})` }} alt="Imagen del producto" />
      <div className={styles.info}>
        <Text
          text={`#${id} ${name}`}
          variant="body"
          color="gray"
        />
      </div>
    </div>
  );
};

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.string),
  onClick: PropTypes.func,
  name: PropTypes.string
};

const defaultProps = {
  classes: {},
  onClick: null,
  name: ''
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

export default Component;
