import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getPathId } from '../../../../utils/helpers';
import { Button } from '../../../ui';
import logo from '../../../../media/Logo.png';
import styles from './Styles.module.css';

const Component = (props) => {

  const history = useHistory();
  const location = useLocation();

  const id = getPathId({
    location: location
  });

  const isHome = location.pathname === '/';

  const handleOnClickGoBack = () => {
    history.push('/');
  }

  const handleOnClickPreviousPokemon = () => {
    const previous = id === '1' ? 151 : parseInt(id) - 1;
    history.push(`/pokemon/${previous}`);
  }

  const handleOnClickNextPokemon = () => {
    const next = id === '151' ? 1 : parseInt(id) + 1;
    history.push(`/pokemon/${next}`);
  }

  return (
    <div className={styles.root}>
      <div className={styles.logo}>
        <img src={logo} alt='logo' />
      </div>
      {
        !isHome &&
        (
          <div className={styles.menu}>
            <div className={styles.left}>
              <Button text='Go Back' onClick={() => handleOnClickGoBack()}/>
            </div>
            <div className={styles.right}>
              <Button text='Previous Pokemon' onClick={() => handleOnClickPreviousPokemon()}/>
              <Button text='Next Pokemon' onClick={() => handleOnClickNextPokemon()}/>
            </div>
          </div>
        )
      }
    </div>
  );
};

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.string)
};

const defaultProps = {
  classes: {}
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

const mapStateToProps = () => ({
});

const ComponentConnected = connect(
  mapStateToProps
)(Component);

export default ComponentConnected;
