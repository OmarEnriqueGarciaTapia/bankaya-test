import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import styles from './Styles.module.css';
import { Text } from '../../../ui';

const Component = () => {

  return (
    <div className={styles.root}>
      <Text text="Developed by Omar Garcia ´https://www.linkedin.com/in/ogarciatapia´." />
    </div>
  );
};

const propTypes = {
  classes: PropTypes.objectOf(PropTypes.string)
};

const defaultProps = {
  classes: {}
};

Component.propTypes = propTypes;
Component.defaultProps = defaultProps;

const mapStateToProps = () => ({
});

const ComponentConnected = connect(
  mapStateToProps
)(Component);

export default ComponentConnected;
