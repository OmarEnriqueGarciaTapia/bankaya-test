import { Layout as DefaultLayout } from './DefaultLayout';
import { Layout as MainLayout } from './MainLayout';

export {
  DefaultLayout,
  MainLayout
};
