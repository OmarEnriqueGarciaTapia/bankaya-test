import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import {
  DefaultLayout,
  MainLayout
} from '../components/layouts';
import {
  Home,
  E404,
  Pokemon
} from '../components/pages';

const Component = () => (
  <Router>
    <MainLayout>
      <Switch>
        <Route exact path="/">
          <DefaultLayout><Home /></DefaultLayout>
        </Route>
        <Route exact path="/pokemon/:id">
          <DefaultLayout><Pokemon /></DefaultLayout>
        </Route>
        <Route path="*"><E404 /></Route>
      </Switch>
    </MainLayout>
  </Router>
);

export default Component;
