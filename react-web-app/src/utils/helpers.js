export const safeGet = (obj, path, defaultValue = null) => {
  if (!obj || !path) return defaultValue;
  try {
    return String.prototype.split.call(path, /[,[\].]+?/)
      .filter(Boolean)
      .reduce((a, c) => (Object.hasOwnProperty.call(a, c) ? a[c] : defaultValue), obj);
  } catch (e) {
    return defaultValue;
  }
};

export const scrollTop = () => window.scrollTo({ top: 0, behavior: 'smooth' });

export const getPathId = ({location}) => location.pathname.split('/').reverse()[0];

export const capitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1);

export const findByTestAtrr = (component, attr) => {
  const wrapper = component.find(`[data-test='${attr}']`);
  return wrapper;
};